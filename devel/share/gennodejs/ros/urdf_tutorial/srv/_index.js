
"use strict";

let changescale = require('./changescale.js')
let changecontrolledjoints = require('./changecontrolledjoints.js')

module.exports = {
  changescale: changescale,
  changecontrolledjoints: changecontrolledjoints,
};
