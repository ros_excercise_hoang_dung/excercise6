Ex 6.1 a

Thay đổi code trong file urdf_tutorial_template.cpp để đăng ký topic teleop_values để điều khiển pan và tilt sử dụng các phím mũi tên.

Lưu code thành urdf_tutorial_a.cpp.

Gợi ý: sử dụng remap để đổi topic turtle1/cmd_vel thành tên teleop_value.

Phân tích:

Nút trái phải thay đổi vận tốc angular.z, sử dụng biến này để xác định vận tốc quay của pan.

Nút lên xuống thay đổi vận tốc của linear.x, sử dụng biến này để xác định vận tốc quay của tilt.

Ex 6.1 b

Thêm một service gọi là urdf_tutorial/change_scale để thay đổi scale trong urdf_tutorial_a. Lưu file code là urdf_tutorial_b.cpp.

Phân tích:

Trong thư mục srv định nghĩa một service changescale với kiểu dữ liệu request là float64.

Viết node server urdf_tutorial_b giống như urdf_tutorial_a nhưng thêm hàm callback khi có request thay đổi scale.

Ex 6.1 c

Thay pan-tilt bởi 7-dof robot. Điều khiển các khớp elbow_pitch_joint và wrist_pitch_joint qua bàn phím.

Lưu code ở file urdf_tutorial_arm.cpp và đặt tên là urdf_tutorial_arm.

Phân tích:

Cần thay đổi robot desciption thành file seven_dof_arm.urdf.

Có thể chỉnh sửa node từ node đã viết ở phần a hoặc b, thay pan_joint bằng elbow_pitch_joint và tilt_joint bằng wrist_pitch_joint, tuy nhiên robot có tới 11 khớp nên cần resize và khai báo các khớp, vị trí khớp vào joint.

Ex 6.1 d
Thêm một service để chọn khớp nào sẽ được điều khiển.

Lưu code ở file urdf_tutorial_arm2.cpp.

Phân tích:

Trong thư mục srv định nghĩa một kiểu service là changecontrolledjoints gồm 2 biến int32 để xác định các khớp được điều khiển.

Khai báo hai dãy là joint[11] và deltajoint[11] để xác định vị trí và chuyển động các khớp. Nếu khớp joint[i] được chọn thì gán deltajoint[i] và joint[i] += deltajoint[i].

Ex 6.1 e

Thêm một TransformListener để giám sát giá trị z của grasping frame và giảm tốc độ điều khiển khi giá trị đó xuống dưới 0.3m
Lưu code vào file urdf_tutorial_arm3.cpp.

Phân tích:

Viết tflistener như ví dụ, nếu tfStamped.Transform.Translation.z mà bé hơn 0.3 thì đặt scale xuống còn 0.1, nếu không thì giữ nguyên giá trị scale cũ.

In ra màn hình một WARN.

Ex 6.1 f

Thay đối cách hoạt động của bài tập 6.1 e như sau: Đặt một vật thể cố định trên mặt phẳng gốc (bằng cách thay đổi file rviz_node.cpp, đặt tên thành rviz_node2.cpp), khi khoảng cách giữa nó và grasping_frame bé hơn 0.5m thì giảm tốc độ xuống.

Có thể vẽ mặt cầu để dễ quan sát giới hạn.

Lưu code trong file urdf_tutuorial_arm4.cpp.

Phân tích:
Em không rõ rviz_node ở đâu nên em thay đổi file urdf, thêm một link mới gọi là Object.

Sử dụng thêm một tflistener cho Object, kiểm tra xem khoảng cách giữa .grasping_frame và Object bằng căn tổng bình phương xem có nhỏ hơn 0.5m không, nếu nhỏ hơn thì giảm scale xuống.

In ra màn hình một WARN.
