#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>


double deltaelbow_pitch;
double deltawrist_pitch;
double scale;
const double degree2rad = M_PI/180;
bool checkCallback = false;

double sign(double x)
{
    if (x==0) return 0;
    if (x>0) return 1.0;else return -1.0;
}

void Callback(const geometry_msgs::Twist& sub_msg)
{
    deltaelbow_pitch = scale*degree2rad*sign(sub_msg.angular.z);
    deltawrist_pitch = scale*degree2rad*sign(sub_msg.linear.x);
    checkCallback = true;
}

bool ChangeScaleServer(
  urdf_tutorial::changescale::Request &req,
  urdf_tutorial::changescale::Response &resp
)
{
    scale = req.s;
    return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  
  //The node advertises the joint values of the elbow_pitch-wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  //Dang ki nhan tin tu topic teleop_value
  ros::Subscriber joint_sub = n.subscribe("teleop_values",1000,&Callback);
  //Khai bao mot Service server
  ros::ServiceServer scale_server = n.advertiseService("urdf_tutorial/change_scale", &ChangeScaleServer);
    
  ros::Rate loop_rate(30);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(11);
  joint_state.position.resize(11);
  double elbow_pitch = 0.0;
  double wrist_pitch = 0.0;

  deltaelbow_pitch = 0.0;
  deltawrist_pitch = 0.0;
  scale =0.5;
  

  while (ros::ok())
  {
      //moving one degree
    //   deltaelbow_pitch =  degree2rad * scale;
    //   deltawrist_pitch = degree2rad * scale;
            
      elbow_pitch = elbow_pitch + deltaelbow_pitch;
      wrist_pitch = wrist_pitch + deltawrist_pitch;
      
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="bottom_joint";
      joint_state.position[0] = 0;
      joint_state.name[1] ="shoulder_pan_joint";
      joint_state.position[1] = 0;
      joint_state.name[2] ="shoulder_pitch_joint";
      joint_state.position[2] = 0;
      joint_state.name[3] ="elbow_roll_joint";
      joint_state.position[3] = 0;
      joint_state.name[4] ="elbow_pitch_joint";
      joint_state.position[4] = elbow_pitch;
      joint_state.name[5] ="wrist_roll_joint";
      joint_state.position[5] = 0;
      joint_state.name[6] ="wrist_pitch_joint";
      joint_state.position[6] = wrist_pitch;
      joint_state.name[7] ="gripper_roll_joint";
      joint_state.position[7] = 0;
      joint_state.name[8] ="finger_joint1";
      joint_state.position[8] = 0;
      joint_state.name[9] ="finger_joint2";
      joint_state.position[9] = 0;
      joint_state.name[10] ="grasping_frame_joint";
      joint_state.position[10] = 0;



      //send the joint state 
      joint_pub.publish(joint_state);
      if (checkCallback)
      {
          checkCallback = false;
          deltaelbow_pitch = 0;
          deltawrist_pitch = 0;
      }

      //Call back
      ros::spinOnce();

      loop_rate.sleep();
  }
  return 0;
}



