#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>


double deltaPan;
double deltaTilt;
double scale;
const double degree2rad = M_PI/180;

double sign(double x)
{
    if (x==0) return 0;
    if (x>0) return 1.0;else return -1.0;
}

void Callback(const geometry_msgs::Twist& sub_msg)
{
    deltaPan = scale*degree2rad*sign(sub_msg.angular.z);
    deltaTilt = scale*degree2rad*sign(sub_msg.linear.x);
}

bool ChangeScaleServer(
  urdf_tutorial::changescale::Request &req,
  urdf_tutorial::changescale::Response &resp
)
{
    scale = req.s;
    return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  
  //The node advertises the joint values of the pan-tilt
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  //Dang ki nhan tin tu topic teleop_value
  ros::Subscriber joint_sub = n.subscribe("teleop_values",1000,&Callback);
  //Khai bao mot Service server
  ros::ServiceServer scale_server = n.advertiseService("urdf_tutorial/change_scale", &ChangeScaleServer);
    
  ros::Rate loop_rate(30);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(2);
  joint_state.position.resize(2);
  double pan = 0.0;
  double tilt = 0.0;

  deltaPan = 0.0;
  deltaTilt = 0.0;
  scale =0.5;
  

  while (ros::ok())
  {
      //moving one degree
    //   deltaPan =  degree2rad * scale;
    //   deltaTilt = degree2rad * scale;
            
      pan = pan + deltaPan;
      tilt = tilt + deltaTilt;
      
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="pan_joint";
      joint_state.position[0] = pan;
      joint_state.name[1] ="tilt_joint";
      joint_state.position[1] = tilt;

      //send the joint state 
      joint_pub.publish(joint_state);

      //Call back
      ros::spinOnce();

      loop_rate.sleep();
  }
  return 0;
}



