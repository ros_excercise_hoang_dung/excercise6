#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <urdf_tutorial/changecontrolledjoints.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>


double deltajoint[11];
double joint[11];
double scale,realscale;
const double degree2rad = M_PI/180;
int c1=4,c2=6;
bool checkCallback = false;

bool checkDistance(geometry_msgs::Vector3 x, geometry_msgs::Vector3 y, double distance)
{
    if (sqrt(pow(x.x-y.x,2)+pow(x.y-y.y,2)+pow(x.z-y.z,2))<0.5) return true;
    else return false;
}

double sign(double x)
{
    if (x==0) return 0;
    if (x>0) return 1.0;else return -1.0;
}

void Callback(const geometry_msgs::Twist& sub_msg)
{
    deltajoint[c1] = realscale*degree2rad*sign(sub_msg.angular.z);
    deltajoint[c2] = realscale*degree2rad*sign(sub_msg.linear.x);
    checkCallback = true;
}

bool ChangeScaleServer(
  urdf_tutorial::changescale::Request &req,
  urdf_tutorial::changescale::Response &resp
)
{
    scale = req.s;
    return true;
}

bool ChangeJointServer(
    urdf_tutorial::changecontrolledjoints::Request &req,
    urdf_tutorial::changecontrolledjoints::Response &resp
)
{
    c1 = req.c1;
    c2 = req.c2;
    return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial_arm3");
  ros::NodeHandle n;
  
  //The node advertises the joint values of the elbow_pitch-wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  //Dang ki nhan tin tu topic teleop_value
  ros::Subscriber joint_sub = n.subscribe("teleop_values",1000,&Callback);
  //Khai bao mot Service server de thay doi scale
  ros::ServiceServer scale_server = n.advertiseService("urdf_tutorial/change_scale", &ChangeScaleServer);
  //Khai bao mot Service server de chon khop dieu khien
  ros::ServiceServer change_joint = n.advertiseService("urdf_tutorial/change_joint", &ChangeJointServer);
  //Khai bao tfListener
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);
  geometry_msgs::TransformStamped tfStamped;

  tf2_ros::Buffer tfBuffer_object;
  tf2_ros::TransformListener tfListener_object(tfBuffer_object);
  geometry_msgs::TransformStamped tfStamped_object;

  ros::Rate loop_rate(30);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(11);
  //Name of joints
  joint_state.name[0] ="bottom_joint";
  joint_state.name[1] ="shoulder_pan_joint";
  joint_state.name[2] ="shoulder_pitch_joint";
  joint_state.name[3] ="elbow_roll_joint";
  joint_state.name[4] ="elbow_pitch_joint";
  joint_state.name[5] ="wrist_roll_joint";
  joint_state.name[6] ="wrist_pitch_joint";
  joint_state.name[7] ="gripper_roll_joint";
  joint_state.name[8] ="finger_joint1";
  joint_state.name[9] ="finger_joint2";
  joint_state.name[10] ="grasping_frame_joint";
  joint_state.position.resize(11);
  
  //Init joint
  for (int i =0; i < 11; ++i){
      joint[i]=0;
      deltajoint[i]=0;
  }
  scale =0.5;
  

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      for (int i=0;i<11;++i){
        // Update position if controlled joint
        if (i == c1 || i == c2) 
        {
            joint[i] += deltajoint[i];
            joint_state.position[i] = joint[i];
        }
      }


      //send the joint state 
      joint_pub.publish(joint_state);

      try{
        tfStamped = tfBuffer.lookupTransform("base_link","grasping_frame", ros::Time(0));
      }
      catch (tf2::TransformException &ex) {
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }
      try{
        tfStamped_object = tfBuffer_object.lookupTransform("base_link","object", ros::Time(0));
      }
      catch (tf2::TransformException &ex) {
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }
      
      //check grasping z value
      if (checkDistance(tfStamped.transform.translation, tfStamped_object.transform.translation,0.5)) // True if distance < 0.5
      {
          realscale = 0.1;
          ROS_WARN_STREAM(std::setprecision(2) << std::fixed << "Distance between Object and Grasping below 0.5m, the movement is slow down ");

      }else{
          realscale = scale;
      }
      //Stop if no message in topic
      if (checkCallback)
      {
          checkCallback = false;
          deltajoint[c1] = 0;
          deltajoint[c2] = 0;
      }

      //Call back
      ros::spinOnce();
      

      loop_rate.sleep();
  }
  return 0;
}



