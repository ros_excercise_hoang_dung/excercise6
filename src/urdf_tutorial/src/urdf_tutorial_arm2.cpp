#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <urdf_tutorial/changecontrolledjoints.h>


double deltajoint[11];
double joint[11];
double scale;
const double degree2rad = M_PI/180;
int c1=4,c2=6;
bool checkCallback = false;

double sign(double x)
{
    if (x==0) return 0;
    if (x>0) return 1.0;else return -1.0;
}

void Callback(const geometry_msgs::Twist& sub_msg)
{
    deltajoint[c1] = scale*degree2rad*sign(sub_msg.angular.z);
    deltajoint[c2] = scale*degree2rad*sign(sub_msg.linear.x);
    checkCallback = true;
}

bool ChangeScaleServer(
  urdf_tutorial::changescale::Request &req,
  urdf_tutorial::changescale::Response &resp
)
{
    scale = req.s;
    return true;
}

bool ChangeJointServer(
    urdf_tutorial::changecontrolledjoints::Request &req,
    urdf_tutorial::changecontrolledjoints::Response &resp
)
{
    c1 = req.c1;
    c2 = req.c2;
    return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  
  //The node advertises the joint values of the elbow_pitch-wrist_pitch
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  //Dang ki nhan tin tu topic teleop_value
  ros::Subscriber joint_sub = n.subscribe("teleop_values",1000,&Callback);
  //Khai bao mot Service server de thay doi scale
  ros::ServiceServer scale_server = n.advertiseService("urdf_tutorial/change_scale", &ChangeScaleServer);
  //Khai bao mot Service server de chon khop dieu khien
  ros::ServiceServer change_joint = n.advertiseService("urdf_tutorial/change_joint", &ChangeJointServer);
    
  ros::Rate loop_rate(30);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(11);
  //Name of joints
  joint_state.name[0] ="bottom_joint";
  joint_state.name[1] ="shoulder_pan_joint";
  joint_state.name[2] ="shoulder_pitch_joint";
  joint_state.name[3] ="elbow_roll_joint";
  joint_state.name[4] ="elbow_pitch_joint";
  joint_state.name[5] ="wrist_roll_joint";
  joint_state.name[6] ="wrist_pitch_joint";
  joint_state.name[7] ="gripper_roll_joint";
  joint_state.name[8] ="finger_joint1";
  joint_state.name[9] ="finger_joint2";
  joint_state.name[10] ="grasping_frame_joint";
  joint_state.position.resize(11);
  
  //Init joint
  for (int i =0; i < 11; ++i){
      joint[i]=0;
      deltajoint[i]=0;
  }
  scale =0.5;
  

  while (ros::ok())
  {
      //moving one degree
    //   deltaelbow_pitch =  degree2rad * scale;
    //   deltawrist_pitch = degree2rad * scale;
            
      
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      for (int i=0;i<11;++i){
        // Update position if controlled joint
        if (i == c1 || i == c2) 
        {
            joint[i] += deltajoint[i];
            joint_state.position[i] = joint[i];
        }
      }


      //send the joint state 
      joint_pub.publish(joint_state);
      //Stop if no message in topic
      if (checkCallback)
      {
          checkCallback = false;
          deltajoint[c1] = 0;
          deltajoint[c2] = 0;
      }

      //Call back
      ros::spinOnce();
      

      loop_rate.sleep();
  }
  return 0;
}



